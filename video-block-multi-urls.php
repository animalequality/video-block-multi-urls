<?php
/**
 * Plugin Name: Gutenberg Video Block Multi Urls
 * Description: core/video & core-embed/youtube Gutenberg blocks extension that provides input for mirrored/alternative sources.
 * Author: Raphaël Droz, Animal Equality
 * Author URI: https://gitlab.com/animalequality/video-block-multi-urls
 * Version: 0.0.1
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: video-block-multi-urls
 * Domain Path: /languages/
 *
 * @package video-block-multi-urls
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_filter(
		'wp_kses_allowed_html',
		function ( $tags ) {
				$tags['figure']['data-urls'] = true;
				return $tags;
		},
		10,
		2
);

add_action(
    'enqueue_block_editor_assets',
    function() {
        // Enqueue our script
        wp_enqueue_script(
            'video-block-multi-urls-js',
            esc_url( plugins_url( '/build/index.js', __FILE__ ) ),
            [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ],
            '0.0.1',
            true // Enqueue the script in the footer.
        );
    }
);

add_filter(
    'render_block',
    function ( $block_content, $block ) {
        static $i = 0;
        $enableMultiUrlOnBlocks = [
            'core/video',
            'core-embed/youtube'
        ];

        if ( in_array( $block['blockName'], $enableMultiUrlOnBlocks, true ) && !empty( $block['attrs']['urls'] ) ) {
            wp_enqueue_script(
                'video-block-multi-urls-handler-js',
                esc_url( plugins_url( '/assets/js/frontend.js', __FILE__ ) ),
                ['jquery'],
                false,
                false
            );
        }

        if ( $block['blockName'] === 'core-embed/youtube' ) {
          // var_dump($block);die;
        }
        if ( $block['blockName'] === 'core-embed/youtube' && !empty( $block['attrs']['urls'] ) ) {
            if (preg_match('!<iframe[^>]+id=!', $block_content)) {
                return $block_content;
            } else {
                $new_html = preg_replace( '!<iframe !', "<iframe id=\"youtube-video-$i\" ", $block_content );
                $i++;
                return $new_html;
            }
        }

        return $block_content;
    },
    10,
    2
);
