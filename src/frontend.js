// See https://stackoverflow.com/questions/9175415/
jQuery(document).ready(function ($) {
        $('figure.wp-block-video[data-urls]').each(function(index) {
                console.log("Adding callback to <video>");
                this.addEventListener('error', (video) => {
                        /**
                         * Other ways to check for errors
                         * - e.target.error.MEDIA_ERR_ABORTED:
                         * - e.target.error.MEDIA_ERR_NETWORK:
                         * - e.target.error.MEDIA_ERR_DECODE:
                         * - e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                         */
                        if (video.target.networkState === HTMLMediaElement.NETWORK_NO_SOURCE) {
                                console.log("video triggered error:", video.target.error, video);
                                var urls = $(this).data('urls');
                                var url = urls.shift();
                                if (url) {
                                        console.log("replacing with ", url.value);
                                        video.target.setAttribute('src', url.value);
                                }

                        }
                }, true);
        });

        if (false && $('figure.wp-block-embed-youtube[data-urls]').length > 0) {
                // window.YTConfig = {'host': 'https://www.youtube.com'};
                console.log("Adding callback to Youtube iframe video(s)");
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }
});

var players = {};
/**
 * This is called, once, after the above www.youtube.com/iframe_api script loaded
 */
function __onYouTubeIframeAPIReady() {
        jQuery('figure.wp-block-embed-youtube[data-urls] iframe').first().each(function(index) {
                // console.log("[onYouTubeIframeAPIReady] found one at ", this.id);
                // console.log(jQuery(this));
                var player = new YT.Player(this.id, {
                        events: {
                                onReady: "onYouTubePlayerReady",
                                onStateChange: "onYouTubePlayerStateChange",
                                onError: "onYouTubePlayerError",
                        }
                        /*
                          host: 'https://www.youtube.com',
                          origin: 'http://us.animalequality.me',
                          widget_referrer: 'http://us.animalequality.me',
                          playerVars: {
                          origin: 'http://us.animalequality.me',
                          widget_referrer: 'http://us.animalequality.me'
                          }*/

                });
                // player.addEventListener("onReady", "onYouTubePlayerReady");
                // player.addEventListener("onStateChange", "onYouTubePlayerStateChange");
                // player.addEventListener("onError", "onYouTubePlayerError");
                players[this.id] = player;
        });
};

function onYouTubePlayerStateChange(event) {
        console.log("onYouTubePlayerStateChange", event);
}

function onPlayerReady(event) {
        console.log("ready", event);
        // event.target.playVideo();
}


function onYouTubePlayerError(e) {
        console.warn(e);
}


callPlayer("youtube-video-0", function() {
        console.log("youpi!!!");
});

function callPlayer(frame_id, func, args) {
    if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
    var iframe = document.getElementById(frame_id);
    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }

    // When the player is not ready yet, add the event to a queue
    // Each frame_id is associated with an own queue.
    // Each queue has three possible states:
    //  undefined = uninitialised / array = queue / .ready=true = ready
    if (!callPlayer.queue) callPlayer.queue = {};
    var queue = callPlayer.queue[frame_id],
        domReady = document.readyState == 'complete';

    if (domReady && !iframe) {
        // DOM is ready and iframe does not exist. Log a message
        window.console && console.log('callPlayer: Frame not found; id=' + frame_id);
        if (queue) clearInterval(queue.poller);
    } else if (func === 'listening') {
        // Sending the "listener" message to the frame, to request status updates
        if (iframe && iframe.contentWindow) {
            func = '{"event":"listening","id":' + JSON.stringify(''+frame_id) + '}';
            iframe.contentWindow.postMessage(func, '*');
        }
    } else if ((!queue || !queue.ready) && (
               !domReady ||
               iframe && !iframe.contentWindow ||
               typeof func === 'function')) {
        if (!queue) queue = callPlayer.queue[frame_id] = [];
        queue.push([func, args]);
        if (!('poller' in queue)) {
            // keep polling until the document and frame is ready
            queue.poller = setInterval(function() {
                callPlayer(frame_id, 'listening');
            }, 250);
            // Add a global "message" event listener, to catch status updates:
            messageEvent(1, function runOnceReady(e) {
                if (!iframe) {
                    iframe = document.getElementById(frame_id);
                    if (!iframe) return;
                    if (iframe.tagName.toUpperCase() != 'IFRAME') {
                        iframe = iframe.getElementsByTagName('iframe')[0];
                        if (!iframe) return;
                    }
                }
                if (e.source === iframe.contentWindow) {
                    // Assume that the player is ready if we receive a
                    // message from the iframe
                    clearInterval(queue.poller);
                    queue.ready = true;
                    messageEvent(0, runOnceReady);
                    // .. and release the queue:
                    while (tmp = queue.shift()) {
                        callPlayer(frame_id, tmp[0], tmp[1]);
                    }
                }
            }, false);
        }
    } else if (iframe && iframe.contentWindow) {
        // When a function is supplied, just call it (like "onYouTubePlayerReady")
        if (func.call) return func();
        // Frame exists, send message
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
    /* IE8 does not support addEventListener... */
    function messageEvent(add, listener) {
        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ?
            w3('message', listener, !1)
        :
            (add ? window.attachEvent : window.detachEvent)('onmessage', listener);
    }
}
