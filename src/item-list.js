const { Component } = wp.element;
const { URLInput } = wp.blockEditor;

class ListItem extends Component {
	constructor() {
		super( ...arguments );
		this.onClickClose = this.onClickClose.bind( this );
		this.onInputChange = this.onInputChange.bind( this );
	}
	onClickClose() {
		this.props.removeItem( this.props.index );
	}
	onInputChange(url) {
                this.props.changeItem(this.props.index, url);
	}

	render() {
		return (
			<li className="list-group-item" onDragOver={() => this.props.onDragOver(this.props.index)}>
                          <button type="button" style={{padding: 0}} className="close drag" onClick={ this.onClickClose }
                                  draggable onDragStart={e => this.props.onDragStart(e, this.props.index)} onDragEnd={this.props.onDragEnd}>
                            &times;
                          </button>
                          <URLInput disableSuggestions={true} isFullWidth={false} onChange={this.onInputChange} value={ this.props.item.value } />
			</li>
		);
	}
}

class List extends Component {
        onDragEnd() {
                this.props.reorderItem(this.draggedIdx, this.draggedItem);
                this.draggedItem = null;
                this.draggedIdx = null;
        };

        onDragStart(e, index) {
                this.draggedItem = this.props.items[index];
                e.dataTransfer.effectAllowed = "move";
                e.dataTransfer.setData("text/html", e.target.parentNode);
                e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
        };

        onDragOver(index) {
                this.draggedIdx = index;
        };

	render() {
		const items = this.props.items.map( ( item, index ) => {
			return (
				<ListItem key={ index } item={ item } index={ index } removeItem={ this.props.removeItem } changeItem={ this.props.changeItem } 
                                          onDragStart={ this.onDragStart.bind(this) } onDragEnd={ this.onDragEnd.bind(this) } onDragOver={ this.onDragOver.bind(this) } />
			);
		} );
		return (
			<ul className="list-group"> { items } </ul>
		);
	}
}

class Form extends Component {
	componentDidMount() {
		this.itemName.focus();
	}

	render() {
		return (
			<div>
				<input type="url" ref={ ( c ) => {
					this.itemName = c;
				} } className="form-control" placeholder="add new..." />
				<button onClick={ () => {
					const newItemValue = this.itemName.value;
					if ( newItemValue ) {
						this.props.addItem( { newItemValue } );
					}
                                    this.itemName.value = '';
				} } className="btn btn-default">Add</button>
			</div>
		);
	}
}

export { List, Form };
