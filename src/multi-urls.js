import assign from 'lodash.assign';
import { List, Form } from './item-list';

const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody } = wp.components;
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableMultiUrlOnBlocks = [
	'core/video',
	'core-embed/youtube'
];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addMultiUrlAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableMultiUrlOnBlocks.includes( name ) ) {
		return settings;
	}

	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = assign( settings.attributes, {
                urls: {
		    type: 'string',
		    /**
                     * This won't work, see
		     * - https://github.com/WordPress/gutenberg/issues/7242 / https://github.com/WordPress/gutenberg/issues/7242#issuecomment-396159013
                     * - https://github.com/WordPress/gutenberg/issues/16164
		     * - https://stackoverflow.com/questions/55949264/#comment104915520_55949264
		     // type: 'array',
                     // source: "attribute",
		     // selector: "figure",
		     // attribute: "data-urls",
		     // default: []
                     */
		    default: '[]',
		},
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'video-block-multi-urls/attribute/urls', addMultiUrlAttribute );

/**
 * Create HOC to add video sources control to inspector controls of block.
 */
const withMultiUrl = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {
		const { setAttributes, attributes } = props;
		var { urls } = attributes;

		// Do nothing if it's another block than our defined ones.
		if ( ! enableMultiUrlOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

                urls = JSON.parse(urls);
		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'Additional sources' ) }
						initialOpen={ true }
					>
						<List items={ urls }
							removeItem={ ( itemIndex ) => {
								urls.splice( itemIndex, 1 );
								setAttributes( {
									date: new Date,
									urls: JSON.stringify(urls),
								} );
							} }
							changeItem={ ( itemIndex, newUrl ) => {
                                                                urls[itemIndex] = {value: newUrl};
								setAttributes( {
									date: new Date,
									urls: JSON.stringify(urls),
								} );
							} }
                                                        reorderItem={ ( index, draggedItem ) => {
                                                                const underlyingItem = urls[index];
                                                                // if the item is dragged over itself, ignore
                                                                if (draggedItem === underlyingItem) {
                                                                        return;
                                                                }

                                                                // filter out the currently dragged item
                                                                let items = urls.filter(item => item !== draggedItem);
                                                                // add the dragged item after the dragged over item
                                                                items.splice(index, 0, draggedItem);
								setAttributes( {
									date: new Date,
									urls: JSON.stringify(items),
								} );
                                                        } } />

						<Form addItem={ ( item ) => {
							urls.push( {
								value: item.newItemValue,
							} );
							setAttributes( {
								date: new Date,
								urls: JSON.stringify(urls),
							} );
						} } />
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withMultiUrl' );

addFilter( 'editor.BlockEdit', 'video-block-multi-urls/with-spacing-control', withMultiUrl );

/**
 * Add margin style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 *
 * See https://github.com/WordPress/gutenberg/issues/16164 about adding data-* attributes.
 *
 */
const addMultiUrlsProp = ( saveElementProps, blockType, attributes ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableMultiUrlOnBlocks.includes( blockType.name ) ) {
		return saveElementProps;
	}

        if ('urls' in attributes) {
                saveElementProps['data-urls'] = attributes.urls; // JSON.stringify()
        }

	return saveElementProps;
};

addFilter( 'blocks.getSaveContent.extraProps', 'video-block-multi-urls/get-save-content/extra-props', addMultiUrlsProp );
